let menuList = document.querySelector('.tabs');
let textList = document.querySelectorAll('.tabs-content>li');
let titleTabs = document.querySelectorAll('.tabs-title');

menuList.onclick = function(event) {
	titleTabs.forEach((elem) => {
		elem.classList.remove('active');
	});

	let target = event.target;

	if (target.className === 'tabs-title') {
		target.classList.add('active');

		textList.forEach((e) => {
			if (e.id === target.innerHTML) {
				e.style.display = 'inline-block';
			} else {
				e.style.display = 'none';
			}
		});
	}
};
