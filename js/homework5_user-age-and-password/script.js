let userFirstName = prompt('Your Firstname', 'Sergey');
let userLastName = prompt('Your Lastname', 'Zorin');
let userBirthDate = prompt(`Your birth date, in 'dd.mm.yyyy' format`, '16.02.1993');

function createNewUser() {
	const newUser = {};
	newUser.firstName = userFirstName;
	newUser.lastName = userLastName;
	newUser.birthDate = userBirthDate;
	newUser.getLogin = function() {
		return `${newUser.firstName[0].toLowerCase()}${newUser.lastName.toLowerCase()}`;
	};

	newUser.getAge = function() {
		let todayDay = new Date().getDate();
		let todayMonth = new Date().getMonth() + 1;
		let todayYear = new Date().getFullYear();

		let userDay = newUser.birthDate.slice(0, 2);
		let userMonth = newUser.birthDate.slice(3, 5);
		let userYear = newUser.birthDate.slice(6, 10);

		let userAge = todayYear - userYear;
		if (userMonth > todayMonth || (userMonth === todayMonth && userDay > todayDay)) {
			userAge--;
		}
		return userAge;
	};

	newUser.getPassword = function() {
		return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthDate.slice(6, 10);
	};

	return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());
