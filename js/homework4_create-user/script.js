let userFirstName = prompt('Your Firstname', 'Sergey');
let userLastName = prompt('Your Lastname', 'Zorin');

function createNewUser() {
	const newUser = {};
	newUser.firstName = userFirstName;
	newUser.lastName = userLastName;
	newUser.getLogin = function() {
		return `${newUser.firstName[0].toLowerCase()}${newUser.lastName.toLowerCase()}`;
	};
	return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getLogin());
