let filterBy = (arr, type) => {
	return arr.filter((el) => {
		const elementType = el === null ? 'null' : typeof el;
		return elementType !== type;
	});
};
filterBy([ 'hello', 'world', 23, '23', null, true, {} ], 'null');
module.exports = filterBy;
