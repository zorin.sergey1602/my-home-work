const cofe = require('chai');
const kakao = require('mocha');
const filterBy = require('./script.js');

const { expect } = cofe;
const { describe, it } = kakao;

describe('filterBy', () => {
	it('should remove strings', () => {
		expect(filterBy([ 1, 2, 'a' ], 'string')).to.deep.equal([ 1, 2 ]);
	});
	it('should remove numbers', () => {
		expect(filterBy([ 1, 2, 'a' ], 'number')).to.deep.equal([ 'a' ]);
	});
	it('should remove null', () => {
		expect(filterBy([ 1, 2, 'a', null ], 'null')).to.deep.equal([ 1, 2, 'a' ]);
	});
	it('should remove boolean', () => {
		expect(filterBy([ 'hello', 'world', 23, '23', null, true, {} ], 'boolean')).to.deep.equal([
			'hello',
			'world',
			23,
			'23',
			null,
			{},
		]);
	});
});
