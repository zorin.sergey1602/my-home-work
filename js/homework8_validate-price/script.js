let input = document.createElement('input');
input.setAttribute('value', 'Price');

document.body.append(input);

const p = document.createElement('p');
p.innerHTML = ' ';
input.after(p);

input.addEventListener('focus', function() {
	input.style.cssText = 'outline:none; border:1px solid green';
});
input.addEventListener('blur', function() {
	let val = +input.value;

	input.style.cssText = 'border:1px solid green';
	const span = document.createElement('span');
	span.style.cssText = 'display:block';
	span.innerText = `Текущая цена: ${val}`;

	const button = document.createElement('button');
	button.innerHTML = 'X';

	span.append(button);
	input.style.cssText = 'color:green';

	if (val <= 0 || typeof val !== 'number' || isNaN(val)) {
		input.style.cssText = 'border:1px solid red';
		p.innerHTML = 'Please enter correct price';
		return;
	} else {
		p.remove();
	}

	input.before(span);
});

document.body.onclick = function(evt) {
	if (evt.target.tagName == 'BUTTON') {
		evt.target.closest('span').remove();
		input.value = 'Price';
	}
};
