const arr = [ 'hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', '1', '2', '3', 'sea', 'user', 23 ];

function createList(arr) {
	let newArr = arr.map((item) => `<li>${item}</li>`);
	let outputToPage = newArr.join('');
	document.body.innerHTML = `<ul>${outputToPage}<ul>`;
}

createList(arr);
